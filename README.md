{{ app_name }}
==========================

Install or update
-------

### latest version ###
```
pip install --upgrade {{ app_name }}
```
### An specific release version ###
```
pip install --upgrade {{ app_name }}==[version]
```
