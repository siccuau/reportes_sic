TABLA SIC_LOG_EVENTOS:
/******************************************************************************/
/****           Generated by IBExpert 06/03/2020 01:02:26 p. m.            ****/
/******************************************************************************/

/******************************************************************************/
/****     Following SET SQL DIALECT is just for the Database Comparer      ****/
/******************************************************************************/
SET SQL DIALECT 3;



/******************************************************************************/
/****                                Tables                                ****/
/******************************************************************************/



CREATE TABLE SIC_LOG_EVENTOS (
    ID              INTEGER,
    FOLIO           VARCHAR(9),
    CAMPO_MODIF     VARCHAR(350),
    VALOR_ANTERIOR  VARCHAR(100),
    VALOR_NUEVO     VARCHAR(100),
    USUARIO         VARCHAR(30),
    FECHA_HORA      TIMESTAMP,
    MODULO	    VARCHAR(10)
);




/******************************************************************************/
/****                              Privileges                              ****/
/******************************************************************************/


/* Privileges of users */
GRANT SELECT ON MON$ATTACHMENTS TO PUBLIC;
GRANT SELECT ON MON$CALL_STACK TO PUBLIC;
GRANT SELECT ON MON$CONTEXT_VARIABLES TO PUBLIC;
GRANT SELECT ON MON$DATABASE TO PUBLIC;
GRANT SELECT ON MON$IO_STATS TO PUBLIC;
GRANT SELECT ON MON$MEMORY_USAGE TO PUBLIC;
GRANT SELECT ON MON$RECORD_STATS TO PUBLIC;
GRANT SELECT ON MON$STATEMENTS TO PUBLIC;
GRANT SELECT ON MON$TABLE_STATS TO PUBLIC;
GRANT SELECT ON MON$TRANSACTIONS TO PUBLIC;

/* Privileges of roles */
GRANT ALL ON SIC_LOG_EVENTOS TO USUARIO_MICROSIP;